\chapter{General Purpose GPU Computing}
\label{chap:background}
%\fixchapterheading % Use this if section follows chapter immediately

This chapter describes essential general purpose GPU (GPGPU) computing background
    that is needed to help understand the rest of the dissertation.
Currently, there are two most widely used GPGPU frameworks: compute
    unified device architecture (CUDA)~\cite{CUDAProgrammingGuide} and
    open computing language (OpenCL)~\cite{OpenCL}.
CUDA is a proprietary framework developed by NVIDIA corporation; OpenCL is a
    public framework designed by Khronos Group.
Despite its proprietary feature, CUDA has several advanced features,
    such as concurrent streaming and flexible memory management which
    are helpful to system-level computing and OpenCL missed at the
    time of my dissertation work.
As a result, CUDA is used in this dissertation to represent the lowest level
    GPGPU computing framework.
And its terminology and concepts are used in this chapter to explain GPGPU
    computing.    
For a comprehensive description of CUDA-based modern GPGPU computing,
    readers may refer to NVIDIA's CUDA programming
    guide~\cite{CUDAProgrammingGuide}.

\section{Overview}
\label{sec:gpu-computing-process}

A GPU works as a coprocessor of the CPU. It has dedicated video memory
    on the card to save the computing data. The processor cores on
    GPU can only access the video memory, so any data to be
    processed by the GPU must be copied into the video memory.
To utilize the GPU, a typical workflow includes three steps:
\begin{enumerate}
  \item CPU code copies the data to be processed from main memory (also
    called ``host memory'' in CUDA) to the video memory (also called
    ``device memory'');
  \item CPU code starts the GPU kernel, which is the program to be
    executed on the GPU, to process the copied data and produce the
    result in the device memory;
  \item After the GPU kernel execution, the CPU code copies the result
    from device memory back to host memory.
\end{enumerate}
Most GPUs sit in the peripheral component interconnect express (PCIe)
    slots, needing DMA over PCIe for the aforementioned memory copy.
The GPU kernel is a program consisting of GPU binary instructions.
Using CUDA, programmers can write C/C++ code, then use \nvcc{} to
    compile them into GPU instructions.
CUDA also provides runtime libraries to allow CPU code to use special
    host memory, device memory, make DMA copy and manage GPU
    execution.
\ignore{OpenCL provides a similar tool set, but some advanced features
    such as concurrent kernel execution and overlapped execution and
    memory copy were not implemented yet at the time of writing this
    dissertation.}
    

\section{Parallel GPGPU}  % level 2
\label{sec:simt}

\begin{figure}[b]
\centering
\includegraphics[width=0.66\columnwidth]{imgs/gpuarch.pdf}
\caption{Architecture of a CUDA GPU.}
\label{fig:gpu-arch}
\end{figure}

A GPGPU is a special single instruction multiple data (SIMD) processor
    with hundreds or thousands of cores and a variety of memory types (as
    shown in Figure~\ref{fig:gpu-arch}).
On recent CUDA GPUs, each 32 cores are grouped into a ``warp.''
All 32 cores within a warp share the same program counter.
A GPU is divided into several ``stream multiprocessors,'' each of which
    contains several warps of cores and fast on-chip memory shared by
    all cores.
A GPU kernel is executed by all the physical cores in terms of
    threads.
So for a given GPU kernel, it becomes one thread on each GPU core when
    executing.
High-end GPU models, such as GTX Titan Black~\cite{TitanBlack}, can
    have as many as 2880 cores.
In that sense, up to 2880 threads can concurrently run on a GPU to
    execute a single GPU kernel.
Threads on GPU cores may do hardware-assisted context switching, in
    case of memory operations or synchronization, which is determined
    and scheduled by the GPU hardware.
Such zero-cost hardware context switching makes it possible to
    run millions of threads on a single GPU without any context
    switching overhead.
Such a ``single kernel executed by multiple threads'' computing model
    is called single instruction multiple threads, or SIMT.

SIMT is a simple and efficient parallel processor design. But
    similar to other SIMD processors, the performance of SIMT may
    suffer from any control flow divergence because of the shared
    program counter within a warp.
On CUDA GPUs, any control flow divergence within a warp may cause
    serialized control flow.\ignore{, such as the following code:
\begin{lstlisting}[language=C]
  if (condition) {
    doSomething();
  } else {
    doSomethingElse();
  }
\end{lstlisting}
The \texttt{if-else} structure, will be serialized by the SIMT
    architecture so that each thread executes both
    \texttt{doSomething()} and \texttt{doSomethingElse()} branches.}
When a core executes the control flow branch that its current thread
    should not jump into, it simply disables memory or register
    access to disable the execute effects.
Different from the powerful sequential cores on CPUs that are armed
    with large cache, out-of-order execution and accurate branching
    prediction, a single GPU core is very weak.
The performance speedup of many GPGPU programs comes from partitioning
    the workload into millions of small pieces processed by thousands
    of lame cores in parallel.    
As a result, a GPU kernel full of such control flow structures
    may severely slow down the GPU performance due to the control
    flow divergence.
  
SIMT architecture requires programmers to carefully tune their
    GPU kernels to avoid conditional structures and to reduce
    as many loops as possible.
This can affect the data processing design in system software. For
    example, when using GPUs to process a batch of network packets
    through the network stack, packets in the batch may diverge to
    different paths because of their different protocols.
Such path divergence will lead to warp divergence
    on GPUs because GPU kernel threads must execute different code for
    packets going to different paths.
As we will see in \snap{}, we have proposed techniques and design
    principles to reduce the overhead caused by this problem.
    
\section{GPU Memory Architecture}
\label{sec:gpu-memory}
The original video memory on GPU board has been largely increased to up
    to 6GB (gigabyte) for a single GPU.    
Compared with the instruction execution on GPU cores, accessing GPU
    video memory, which is also called global memory, is much slower and
    may cost hundreds of cycles.
So global memory access is a time consuming operation that should be
    minimized.
Fortunately, the GPU memory bus width is much wider than normal CPUs,
    e.g., some model~\cite{TitanBlack} has a 384 bits
    memory bus width which leads to 336 GB/s bandwidth.
Though a single thread may not be able to fully utilize the 384 bits
    bus, a SIMT-oriented GPU kernel may take advantage of the wide bus
    by issuing memory access operations to consecutive locations from
    the same warp, which is called ``memory access coalescing.''

Besides the global memory, there are some special memory types on GPUs for
    fast memory access in certain applications (as shown in
    Figure~\ref{fig:gpu-arch}). 
Constant memory is a small region of memory that supports fast
    read-only operations.    
Texture memory is similar to constant memory but has very fast special
    access patterns. For example, according to the
    Kargus~\cite{Kargus} network intrusion detection system (NIDS),
    using texture memory for deterministic finite atomata (DFA)
    transition table improves the pattern matching performance by
    20\%.
Shared memory can be viewed as a program-controlled cache for a block of
    threads. Its access latency can be 100x lower than the uncached
    global memory.
In fact, current CUDA GPUs use a single region of 64KB (kilobyte)
    on-chip memory per stream multiprocessor to implement both layer
    one (L1) cache and shared memory, allowing either 48 KB cache and
    16 KB shared memory or 16 KB cache and 48 KB shared memory split
    that is configurable by programmers.
Besides program-controlled cache for fast frequent data access, shared
    memory can also be used to achieve global memory access
    coalescing, e.g., when using four threads to encrypt a single
    16-byte advanced encryption standard (AES) data block, each
    thread can issue a four-byte read operation to be coalesced into a
    single sixteen-byte transaction.

As a result, to achieve high performance GPU computing, a GPU program
    should be designed and executed with the following memory related
    considerations: consecutive threads should issue coalescable
    memory accesses; memory
    access latency can be hidden by launching more threads than cores
    in order to switch out the threads that wait for memory
    transaction; read-only data should go into constant or texture
    memory; frequently accessed data should go into shared memory.
    
\section{Advanced GPGPU Computing}
\label{sec:advanced-gpgpu}

GPUs have been designed to be more than simple coprocessors.
Many advanced techniques have been applied to improve the GPU
    computing environment.
In the hardware perspective: GPUs are capable of having multiple GPU
    kernels executed concurrently on their cores; some high-end GPUs
    have more than one DMA engine on a single GPU board; the rapidly
    development of dynamically random access memory (DRAM) technology
    has significantly reduced the cost of large volume of DRAM in
    modern computers, so using large trunks of nonpageable dedicated
    physical memory for DMA is feasible.
To utilize these hardware features to improve the GPU computing
    performance, CUDA has implemented and exposed software interfaces
    for programmers to access them.
    
CUDA provides the ``stream'' abstraction, which represents a GPU
    computing context consisting of a DMA engine for memory copy, and
    GPU cores for GPU kernel execution.
CUDA GPUs support multiple concurrent streams, which essentially
    enables concurrent execution of multiple GPU kernels on a single
    GPU, and utilizes multiple DMA engines for concurrently
    bidirectional PCIe transfers.
Each stream is independent of the other: operations (memory copy and
    GPU kernel execution) in the same stream are sequentially performed, but
    operations in different streams are totally independent.
This provides an asynchronous streaming GPU computing model, which may
    fully utilize the execution and copy capabilities of GPUs.
    
DMA memory copy requires nonpageable memory pages that are locked in
    physical memory.
So if a program wants to copy the data in pageable memory with GPU
    DMA, the GPU driver has to firstly allocate an extra locked memory
    buffer, then copy the data from pageable memory to locked memory,
    and finally copy the data from locked memory to the GPU with DMA.
This causes a double-buffering problem that causes an extra copy in
    host memory and also wastes extra memory space.
As a result, a program should use locked memory directly to store its
    data in order to avoid this double-buffering problem in GPU DMA.

\section{Efficient GPGPU Computing Techniques}
\label{sec:gpgpu-techniques}

To achieve efficient GPGPU computing, the aforementioned GPU features
    must be considered to design GPU kernels and to design the host
    side software.
\begin{itemize}
  \item SIMT architecture requires as few branches as possible in GPU
    kernel's control flow to avoid warp divergence. This is more an
    algorithmic requirement than a system one. However, as the packet
    processing example shows in Section~\ref{sec:simt}, sometimes the
    system-level processing control flow can cause the GPU side
    divergence, hence it requires the system design to be GPU-aware,
    to either avoid processing flow divergence, or apply techniques
    to reduce the divergence overhead like in \snap{}.
    
  \item The wide memory bus requires coalescable memory accesses in
    consecutive threads. This not only requires SIMT GPU kernel to
    guarantee coalescable memory accesses from consecutive threads,
    but also needs designing coalescing-friendly data structures or
    data layout in system-level software.
    
  \item The variety of different GPU memory requires GPU kernels to be
    carefully optimized and tuned to make use of the faster read only
    memory regions and shared memory. This is mostly a general
    algorithmic requirement, not a very ``system'' need. But
    as said in Section~\ref{sec:gpu-memory}, system code still
    needs finely tuned system-level algorithms such as Kargus'
    DFA table placement in texture memory that improves 20\% pattern
    matching performance.
    
  \item The CUDA stream technology requires programmers to make use of
    the concurrency and bidirectional PCIe bus with careful design.
    This needs system designers to take advantage of concurrent
    workloads or partitioning large single-threaded workload, properly
    abstracting the computations on GPUs to utilize the overlapped GPU
    computation and memory copy pipeline enabled by CUDA stream.
    
  \item The page-locked memory requires the programmers to design
    their memory management mechanism with the consideration of GPU
    resources at the very beginning. Because memory management in
    system-level, especially in OS components can be very complex.
    System components can either have their own memory allocators, or
    more often, just work as stages in a pipeline that don't own the
    memory of the processed data buffers at all.
    
\end{itemize}

Traditional GPGPU computing considers mostly the GPU side optimization
    techniques such as the SIMT architecture, wide memory bus and
    different types of device memory.
Their workflows are often very simple, as shown in
    Section~\ref{sec:gpu-computing-process}.
For these kind of traditional GPGPU programs, all the complex
    low-level problems such as memory management, memory copy
    efficiency, bus bandwidth, device synchronization
    overhead, interaction with other system components, are hidden and
    handled efficiently by the system-level code.
Now when it comes to the system-level software targeted in this
    dissertation, all of them must be considered in the
    system design when integrating GPU computing.
Special system-level data usage patterns may cause large PCIe bus
    bandwidth waste when copying memory, and may also cause difficulty
    to issue coalescable memory accesses from GPU threads.
Using page-locked memory may be not as simple as calling the CUDA memory
    allocator; it may lead to complex memory page remapping in
    system code.
Some inherently asynchronous system codes may not be able to afford the host-device
    synchronization because that will change their efficient ways of
    working.
All of these system-level issues must be handled in this dissertation work,
    as will be shown in the next chapter.

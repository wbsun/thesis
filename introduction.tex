\chapter{Introduction}
\label{chap:introduction}
%% \fixchapterheading % Use this if section follows chapter immediately

System-level software sits at the bottom of the software stack,
    including file systems, device drivers, network stacks, and many
    other operating system (OS) components.
They provide basic but essential storage, communication and security
    functionalities for upper-level applications.
It is vital for system software to make those functionalities
    efficient and scalable, otherwise the entire software stack will
    be slowed down.
Yet, many system-level functionalities require substantial computing
    power.
Examples include encryption for privacy, deep packet inspection for
    network protection, error correction code or erasure
    code for fault tolerance, and lookups in complex data structures
    (file systems, routing tables, or memory mapping structures).
All of these may consume excessive processing power.
What's more, today's application workloads are dramatically
    increasing: gigabytes or even terabytes of multimedia contents,
    high definition photos and videos, tens or even hundreds of gigabits
    per second network traffic and so on.
Thus more and more computing power is needed to process those
    bulk-data workloads on modern rich functional software stacks.

A very common and important feature of many system-level computational
    functionalities is that they are inherently
    parallelizable. Independent processing can be done in parallel at
    different granularities: data blocks, memory pages, network packets,
    disk blocks, etc.
Highly parallel processors, in the form of graphics processing units
    (GPUs), are now common in a wide range of systems: from tiny
    mobile devices up to large scale cloud server
    clusters~\cite{AWS-GPU} and super
    computers~\cite{GTC13:GRID}.
Modern GPUs provide far more parallel computing power than multicore
    or many-core central processing units (CPUs): while a CPU may have
    two to eight cores, a number that is creeping upwards, a modern
    GPU may have over two thousands~\cite{TitanBlack}, and the number
    of cores is roughly doubling each
    year~\cite{CUDAProgrammingGuide}.
As a result, exploiting parallelism in system-level functionalities to
    take advantage of today's parallel processor advancement is
    valuable to satisfy the excessive demand of computing power by
    modern bulk-data workloads.

GPUs are designed as a \emph{throughput-oriented
    architecture}~\cite{TOA}: thousands of cores work together to
    execute very large parallel workloads, attempting to maximize the
    total throughput, by sacrificing serial performance.
Though each single GPU core is slower than a CPU core, when the
    computing task at hand is highly parallel, GPUs can provide
    dramatic improvements in throughput.
This is especially efficient to process bulk-data workloads, which
    often bring more parallelism to fully utilize the thousands of GPU
    cores. 

GPUs are leading the way in parallelism: compilers~\cite{gpu-comp1,
    gpu-comp2, gpu-comp3}, algorithms~\cite{gpu-algo1, gpu-ds1,
    gpu-algo2}, and computational models~\cite{kmodel, model2,
    a-model} for parallel code have made significant advances in
    recent years.
High-level applications such as video processing, computer graphics,
    artificial intelligence, scientific computing and many other
    computationally expensive and large scale data processing tasks have
    benefited with significant performance
    speedups~\cite{GPU-Survey, GPU-SurveyNew} from the advancement
    of parallel GPUs.
System-level software, however, have been largely left out of this
    revolution in parallel computing.    
The major factor in this absence is the lack of techniques for how
    system-level software should be mapped to and executed on GPUs.
    
For high-level software, GPU computing is fundamentally based on a
    computing model derived from the data parallel computation such as
    graphics processing and high performance computing (HPC).
It is designed for long-running computation on large datasets, as
    seen in graphics and HPC.
In contrast, system-level software is built, at the lowest level of
    the software stack, on sectors, pages, packets, blocks, and other
    relatively small structures despite modern bulk-data workloads.
The scale of the computation required on each small structure is
    relatively modest.
Apart from that, system-level software also has to deal with very low-level
computing elements, such as memory management involving pages, caches,
    and page mappings, block input/output (I/O) scheduling, complex
    hardwares, device drivers, fine-grained performance tuning, memory
    optimizations and so on, which are often hidden from the
    high-level software.
As a result, system-level software requires technologies to bridge the
    gap between the small building structures and the
    large datasets oriented GPU computing model; also to properly
    handle those low-level computing elements with careful design
    trade-offs and optimizations, to take advantage of the 
    parallel throughput-oriented GPU architecture.

This dissertation describes the generic principles of system-level GPU
    computing, which are abstracted and learned from designing,
    implementing and evaluating general throughput-oriented GPU
    computing models for two representative categories of system-level
    software: storage applications and network packet processing
    applications.
Both models are in the form of general frameworks that are designed
    for seamlessly and efficiently integrating parallel GPU computing
    into a large category of system-level software.
The principles include unique findings of system-level GPU computing
    features, and generic design techniques and abstractions to deal
    with common system-level GPU computing challenges we have identified.
The significant performance improvements in storage and network packet
    processing applications brought by integrating GPU computing with
    our frameworks shows the effectiveness and efficiency of the
    proposed techniques and abstractions, and hence supporting the
    following statement:    
    
\section{Dissertation Statement}
\label{sec:statement}
%% \begin{quote}
  \emph{The throughput of system software with parallelizable,
    computationally expensive tasks can be improved by using GPUs and
    frameworks with memory-efficient and throughput-oriented designs.}
%% \end{quote}
    
\section{Contributions}
\label{sec:contributions}
The contributions of this dissertation work include the following.
\begin{itemize}
  \item Two general frameworks, \gpustore{}~\cite{GPUstore} and
    \snap{}~\cite{Snap}, for integrating parallel GPU computing into
    storage and network packet processing software, as described in
    Chapter~\ref{chap:gpustore} and Chapter~\ref{chap:snap}.
  \item Three high throughput GPU-accelerated Linux kernel storage
    components, including a filesystem and two storage device mappers,
    which are discussed in Chapter~\ref{chap:gpustore}.
  \item A set of Click~\cite{Click} elements for \snap{} to help build
    parallel packet processing pipelines with GPUs, which are
    described in Chapter~\ref{chap:snap}.
  \item A modified fully functional Click Internet protocol (IP)
    router with fast parallel GPU acceleration built on top of \snap{}
    as described in Chapter~\ref{chap:snap}.
  \item A literature survey of system-level GPU computing that covers
    existing work, potential applications, comparison of the useful
    techniques applied in surveyed work and the ones proposed in our
    work as presented in Chapter~\ref{chap:survey}.
\end{itemize}


\section{Findings}
\label{sec:finding}
Besides the above contributions, we also have found and learned
    valuable facts and lessons from designing and implementing
    \gpustore{} and \snap{}.
We believe they are common and applicable to other system-level GPU
    computing software, too.
We will thoroughly discuss them in the later chapters.
For now, they are listed below as an overview.
\begin{itemize}
  \item \textbf{Batching improves GPU utilization.} System code is
    often latency-oriented. As a result, system software often works
    with small building blocks. However, GPUs' architecture is
    throughput-oriented. Small building blocks lead to constant
    overhead and low GPU utilization. To bridge the gap between the
    latency-oriented system code and throughput-oriented GPUs,
    batching small blocks to process many at once amortizes the
    overhead and improves GPU utilization. Batching may increase the
    processing latency of a single block, but for systems dealing with
    I/O, adding relatively small latency is tolerable.
  \item \textbf{High throughput computing needs truly asynchronous GPU
    programming.} With the required synchronization, current GPU
    programming models are mismatched to asynchronous system
    components, which widely exist in file systems, block I/O, device
    drivers, and network stacks at system level. Synchronization
    stalls the typical GPU computing pipeline which consists of three
    common stages: (1) host-to-device direct memory access (DMA) copy; (2) GPU kernel
    execution; (3) device-to-host DMA copy.  Event-based or
    callback-based asynchronous GPU programming is necessary for
    asynchronous systems code, allowing it to fully utilize the GPU
    pipeline and achieve high throughput.
  \item \textbf{System code has data usage patterns that are different
    from traditional GPU code.}
    Traditional GPU computing usually reads the data from a file or the
    network into host memory, copies the entire data
    buffer into GPU device memory, and does the computation based on
    all the data. However, the code in a system component often
    works as a stage of a long data processing pipeline, and may use
    either the entire dataset passed through the pipeline, or just a
    few small pieces. In order to improve the system
    performance, special care must be taken to provide
    different memory models to system code (both the CPU side code
    and the GPU side code) according to the code's data usage pattern.
    \begin{itemize}
      \item \textbf{The computation needs all the data.}
        This data usage pattern is similar to traditional GPU
        computing. In this case, the GPU needs the entire dataset to be
        copied into GPU memory. To
        reduce the memory copy overhead,
        we should focus on reducing the copy within host memory. Different
        from traditional GPU computing, in the system-level
        context, processing stages often pass data buffers owned by a
        third party, such as the page-cache managed memory in the
        storage stack and network packet buffers in the network stack,
        through a 
        long pipeline. Remapping the memory to 
        make it GPU DMA-capable avoids redundant memory copy in host
        memory.    
      \item \textbf{The computation uses just a few small pieces of
        the large trunk of data.} In contrast to most GPU computing,
        some system-level computing tasks such as packet processing use
        only a few small pieces of the entire dataset. In this case,
        copying the entire dataset into GPU memory wastes a
        large portion of the bus bandwidth. Considering the much
        higher host memory bandwidth than the bus bandwidth, it is worth copying
        the scattered small pieces into a consecutive host memory
        buffer, allowing one smaller DMA copy to
        GPU. Such trade-off takes advantage of the faster host
        memory to reduce the overall memory copy time. Gathering the
        scattered pieces together also benefits memory access
        coalescing on the GPU, taking advantage of the wide GPU memory
        interface.
    \end{itemize}
\end{itemize}


\section{Dissertation Outline}
\label{sec:overview}

Chapter~\ref{chap:background} gives a simple background introduction
    of concepts, techniques and features of general purpose GPU
    computing. This chapter tries to get readers without GPU computing
    background familiar with it, and also defines the terminology used
    in later chapters. Readers familiar with traditional GPU
    computing can skip this.

Chapter~\ref{chap:systemgpu} describes the system-level GPU computing
    principles at the high level. It discusses the challenges and
    special requirements in system-level GPU computing and the
    proposed techniques and abstractions to deal with them.

Chapter~\ref{chap:gpustore} describes the storage framework
    \gpustore{}, including the design, implementation and the
    experimental evaluation. The GPU-accelerated file system
    and block device drivers built on top of \gpustore{} have achieved
    up to an order of magnitude performance improvements compared with
    the mature CPU-based implementations.

Chapter~\ref{chap:snap} describes the network packet processing
    framework \snap{}, similar to the previous \gpustore{}, including
    the design, implementation and the experiments. The demonstrated
    deep packet inspection router built with \snap{} has shown
    40\,Gbps (gigabits per second) line rate packet processing
    throughput at very small packet size.

Chapter~\ref{chap:survey} is the survey of system-level GPU computing,
    which includes both existing work and the identified
    possible application areas. The survey discusses techniques used
    by other system-level GPU systems to compare with what this dissertation
    has proposed and applied.

Chapter~\ref{chap:conclusion} reviews the dissertation and concludes.


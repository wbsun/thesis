\chapter{System-level GPU Computing Principles}
\label{chap:systemgpu}
%\fixchapterheading % Use this if section follows chapter immediately

\ignore{The combined parallel GPUs and sequential CPUs have become the most
    widely available heterogeneous computing platform.
GPUs can be found in a variety of computing systems: from tiny little
    hand-held mobile phones, tablet, mini-PCs, embedded boards, to
    laptops, desktops, servers, data center clusters and super
    computers.
Compared with the $200$ to $300$\,gigaFLOPS high end CPUs, consumer
    level GPUs can reach as high as $5.1$\,teraFLOPS, and cost much
    less than CPUs~\cite{TitanBlack, CUDAProgrammingGuide}.
This represents a large amount of computing power that the system
    software should not overlook.}

A lot of system-level workloads are good candidates of the
    throughput-oriented GPU accelerations:
file systems and storage device layers use encryption, hashing,
    compression, erasure coding; high bandwidth network traffic
    processing tasks need routing, forwarding, encryption, error
    correction coding, intrusion detection, packet classification; a
    variety of other computing tasks perform virus/malware detection,
    code verification, program control flow analysis, garbage
    collection and so on.
Not every system-level operation is capable of throughput improvement
    with GPUs. Those inherently sequential or latency sensitive tasks
    such as process scheduling, acquiring current date-time, getting
    process identifier (ID), setting up I/O devices, allocating memory
    and so on definitely can't afford the relatively high-latency GPU
    communication that often crosses external buses.
    
Some early system-level work has demonstrated the amazing performance
    speedup using GPUs:
PacketShader~\cite{PacketShader}, a GPU-accelerated software router,
    is capable of running IP routing lookup at most 4x faster than the
    CPU-based mode.
SSLShader~\cite{SSLShader}, a GPU-accelerated secure sockets layer
    (SSL) implementation, runs four times faster than an equivalent
    CPU version.
Gnort~\cite{Gnort}, as an GPU-accelerated NIDS, has showed 2x speedup
    over the original CPU-based system.
EigenCFA~\cite{EigenCFA}, a static program control flow analyzer, has
    reached as much as 72x speedup when transforming the flow analysis
    into matrix operations on GPUs.

However, all of them are very specialized systems that have been done
    in an ad hoc way to deal with performance obstacles such as memory
    copy overhead, parallel processing design and those mentioned in
    the previous chapter, without considering any generic
    systematic design for a wide range of related applications.
A general system-level GPU computing model needs to provide generic
    design principles.
It requires a thorough study of system-level behaviors and GPU
    computing features to identify the common challenges when
    integrating GPUs into low-level system components, and then come
    up with generic techniques and abstractions to decompose the
    system, trade off design choices according to contexts,
    and finally solve the problems.

This chapter discusses the generic challenges faced by system-level
    GPU computing and our proposed design principles.
Similar to traditional general purpose GPU computing, tuning GPU
    kernel to make parallel algorithms fully utilize the SIMT cores is
    important to system-level code, too.
However, a unique feature of system-level computing tasks is that most
    of them are much simpler than the application-level tasks in
    scientific computing and graphics processing.
If we take a look at the surveyed computing tasks in system-level in
    Section~\ref{sec:applications-survey}, they are mostly one-round
    deterministic algorithms without any iterative scheme and
    convergence requirement such as in partial differential equation
    (PDE) algorithms and randomized simulations.
In that case, other noncomputational costs may become major overhead: for example, as we
    evaluated for \gpustore{}, the
    significant AES performance improvements with a variety of
    memory copy optimization techniques show that the memory copy,
    rather than the cipher computation, is the bottleneck.
As a result, system-level GPU computing is faced with challenges from
    not only traditional device side GPU kernel optimization, but also
    the host side GPU computing related elements.
The following sections discuss each of the major challenges we've
    identified in the \gpustore{} and \snap{} projects and the generic
    techniques to deal with them.
\ignore{As for the design and implementation details of these techniques in
    concrete systems, readers should refer to Chapter~\ref{chap:gpustore}
    and Chapter~\ref{chap:snap}.}

\section{Throughput-oriented Architecture}  % level 2
\label{sec:simt-arch-ch}

The SIMT GPU architecture enables simple but highly parallel computing
    for large throughput processing.
But the SIMT architecture is also a big challenge to GPU kernel
    design and GPU computing workload.
As for the GPU kernel design, it means avoiding control flow
    divergence via tuned GPU code or SIMT-friendly data structures to
    process.
To fit the SIMT architecture, the workload must be able to provide enough
    parallelism to fully utilize the thousands of parallel SIMT cores.
The parallelism may come from the parallelized algorithms in terms of
    GPU kernels. More importantly, it comes from the amount of data to
    be processed in one shot, which often requires batched processing
    in system environments.
When trying to provide a generic design for a variety of system-level
    applications, a right workload abstraction that covers the various
    processing tasks is needed to guarantee the parallelism, and also
    to make it easy to design divergenceless GPU kernels.


\subsection{Parallel Workload Abstraction}
\label{sec:parallelism-in-workloads}

\ignore{The most important thing to utilize parallel GPUs is to ensure
    parallelism in workloads.}
Considering the thousands of cores on a modern GPU that processes one
    basic data unit in one thread, at least the same number of basic
    units is required to fully utilize all the cores.
This is actually the key to achieve high throughput in GPU computing.
Taking the GTX Titan Black~\cite{TitanBlack} as an example: even
    with the parallelized AES algorithm that uses four threads to
    process a single 16-byte block, its 2880 cores still need
    $2880 \times 4 = 11KB$ data to process in one GPU kernel launch.
If we consider the context switching due to global memory access, even
    more data are required to keep GPU cores busy.

Unfortunately, system-level software is often designed to process data
    sequentially due to the latency-oriented system design philosophy
    through the ages.
However, the performance of parallel and sequential processors have
    been significantly improved to be easily achieve very low latency,
    and also today's ``big data'' workloads focus more on high
    throughput processing, even at the system-level.
\ignore{To bridge the gap between the throughput-oriented parallel GPUs and
    the sequential system-level software, batched workload is often
    needed.}
Batching is a simple yet effective technique to accumulate enough
    parallelism in one-shot workload.
Instead of a single data unit to be processed at one time, a one-shot
    workload with batching now includes enough units to fully
    occupy the thousands of GPU cores.
``Enough'' is ambiguous in the high-level workload abstraction.
An intuitive but effective policy for concrete design is to ensure
    that processing the batched workload on GPUs is at least faster than on
    CPUs.
    
An obvious drawback of batching is the increased latency.
This may cause a serious performance problem in some latency sensitive
    software, especially the network applications such as video
    conference, video streaming, etc.
As we will see in \snap{}, the packet processing latency
    introduced by batching is eight to nine times larger than the
    nonbatching latency.
So batching is not a pervasive solution to adapt sequential software
    with the parallel GPUs.
This also reveals the fact that not every system-level software can
    benefit from parallel GPUs, such as those latency sensitive
    computing tasks whose algorithms can't be efficiently
    parallelized.
    

\subsection{Avoid Control Flow Divergence}
\label{sec:avoid-cf-divergence}

Many research projects~\cite{CUDABestPractice, GPUSort, GPUGraph,
    GPULinearAlgebra, GPUPrefixSum, GPUScanReduction, GPUCompaction}
    have been focused on designing efficient SIMT style parallel
    implementation of particular algorithms on the GPU.
Besides those well-studied techniques, how to efficiently
    partition the workload into SIMT threads and how to choose
    SIMT-friendly algorithms and data structures (if possible) are also
    very important.

The workload partitioning is often very straightforward: each GPU
    thread processes one single basic data unit.
Many system-level computing tasks can use such simple partitioning
    scheme such as computing erasure code of one disk sector
    per thread, encrypting a single sixteen-byte AES block per thread,
    looking up next hop for one IP address per thread, etc.
However, exceptions always exist.
The AES algorithm is an example, which has been parallelized by
    exploiting the intrablock parallelism~\cite{ParallelAES}.
\gpustore{} applied this technique in its AES implementation to have
    four threads to encrypt/decrypt a single AES block.    
    
Designing the data structures used by processing logic to make them
    SIMT-friendly is another technique to avoid control flow
    divergence.
One example is the pattern matching algorithm in \snap{} that is based
    on DFA.
The DFA matching is a very simple loop: get the current symbol; find
    the next state indexed by the symbol in current state's transition
    table; use the found state as the current state.
The algorithm itself can be implemented in SIMT style because all
    input strings use the same three-step matching.
But for the second step, some transition table data structures
    cannot ensure same code execution when finding the next state.
For example, a tree-based map may need different lookup steps for
    different symbols to find the mapped states, which means different
    GPU threads may have to execute different numbers of lookup loops
    to find the next states, and hence is not SIMT-friendly.
In the meanwhile, an array-based transition table, which assigns state
    for every symbol in the DFA alphabet, can guarantee equal steps to
    find the next state of any given symbol, and hence is SIMT-friendly.
That's because we can use the symbol as the array index to fetch the
    mapped state, and one state lookup becomes a single array access.
So although the tree-based map is more memory-efficient and the
    array-based map is very memory-consuming because even invalid
    symbols in the alphabet have transition states, sometimes the
    memory may be sacrificed to achieve SIMT-friendly GPU code.
    
\subsubsection{When It Is Unavoidable}
\label{sec:when-it-is-unavoidable}

An effective but not efficient way that can always solve the control
    flow divergence is to partition the computing tasks into multiple
    GPU kernels, each GPU kernel works for a particular control flow branch of
    the computing task.
This needs some information available at the host side to indicate how
    many threads a GPU kernel should be executed by, and which data items
    should be processed by a particular GPU kernel.
Such host side information implies a device-to-host memory copy,
    the necessary host-device synchronization for its completion and
    probably a data structure reorganization,
    which may cost more than admitting divergence at the GPU side.
So it is not always unacceptable to introduce divergence into a GPU
    kernel.
The batched packet processing in different GPU elements in \snap{} is
    a very representative example.
\ignore{\snap{} has evaluated the partition-based approach and the
    divergence-based approach for packet forward, and saw
    \todo{fill out the percentage} performance slow down with the
    former one (see Section~\ref{sec:snap-eval}).}

However, allowing divergence in GPU kernel doesn't mean writing
    arbitrary code; we still need to minimize the effects.
We have proposed and implemented the predicated execution to minimize
    the divergence affections.
More details of the predicated execution are in
    Section~\ref{sec:divergence}.    
    \ignore{
achieve the
    following goals for minimizing the divergence affects:
    \begin{itemize}
      \item \textbf{As few branches as possible:} This is obvious: we only
        allow necessary divergence, and should organize the code to
        minimize the branches.
      \item \textbf{Creating ``empty'' branch:} This can be illustrated by the
        following CUDA kernel code snippet:
        \begin{lstlisting}[language=C]
void TheGPUKernel( ... ) {
  if (shouldRun[threadId]) {
    ... /* do processing */
  }
}
        \end{lstlisting}
        The \texttt{if} statement has an empty \texttt{else} branch,
        which means even if the \texttt{if} statement causes warp
        divergence, the overhead to execute the other branch is very
        small because no instructions in that branch at all.
        So a GPU kernel that can't avoid divergence should consider
        organize its code to such style.
    \end{itemize}}

\section{Coalesce Global Memory Accesses}
\label{sec:coalescing-global-memory-accesses}

The throughput-oriented GPU architecture is armed with not only high
    computation throughput, but also high memory access throughput.
To fully utilize the 384 bits global memory bus in GTX Titan
    Black~\cite{TitanBlack}, coalescable memory accesses must be
    issued from neighbor threads in a thread block.
Compared with noncoalesced access, coalesced memory access can
    achieve orders of magnitude faster~\cite{CUDABestPractice}.    
So the GPU kernel must be designed to access consecutive memory
    locations from threads within the same thread block or warp.
Besides the GPU kernel code, the data structures to be processed must
    be carefully organized to make them coalescing-friendly.
Some kinds of workloads are quite easy to satisfy the coalescing
    requirement, for example, data read from a block device are
    naturally consecutively stored, hence when encrypting them,
    neighbor GPU threads can always coalesce their memory accesses.
However, some workloads are not that coalescing-friendly, needing a
    coalescing-aware abstraction to achieve a generic solution.
A simple but very good example is the IP routing lookup.
The lookup GPU kernel needs only the destination IP address of a packet.
If we put the entire packet into the global memory for each thread to
    access, the memory reads for IP addresses issued from neighbor
    threads will be scattered. One 384 bits memory transaction may only
    read 32 bits effective data, which wastes more than
    $90\%$ memory bandwidth.
But if we organize the destination IP addresses into a separate
    buffer, then up to 12 memory reads for the IP addresses issued
    from neighbor threads can be coalesced into a single memory
    transaction, which significantly reduces the number of global
    memory transactions.
The ``region-of-interest''-based (ROI) slicing technique applied in
    \snap{} is an effective abstraction of the workload data to build
    such coalescing-friendly data structures for a variety of
    GPU-accelerated computating tasks (see Chapter~\ref{chap:snap}).

\section{Overlapped GPU Operations}
\label{sec:overlapped-computing-memcpy}

The typical GPU computing workflow mentioned in
    Section~\ref{sec:gpu-computing-process} can be pipelined to
    improve the utilization of the two major GPU components: SIMT
    cores and DMA engines.
The pipelined model needs multiple CUDA streams, each of which carries
    proper-size workloads for certain computing tasks.    
This may require the workload to be split into multiple trunks in order to
    fill into multiple streams.
The host code will be responsible to do such workload splitting,
    which needs the task-specific knowledge to ensure the
    splitting is correct.
For a generic GPU computing framework such as \gpustore{},
    it is impractical to put those splitting knowledge of every task
    into the generic framework.
The computing tasks need a right abstraction to describe both the
    task-specific logic and the task management (such as
    partitioning) logic.
For example, \gpustore{} provides the modular GPU services, which are
    the abstraction of computing tasks.
Each GPU service not only processes computing requests issued from
    \gpustore{} clients, but also does service-specific request
    scheduling including workload splitting and merging.

This requirement seems totally contrast to the parallelism one in
Section~\ref{sec:parallelism-in-workloads}.
However, it is not a paradox.
An application usually needs to find the balance point of these two
    requirements to decide the optimal size of the workload to be
    processed on GPUs in one shot, so as to achieve the best
    performance.
This can be done either dynamically or statically.
A static approach finds the balance point offline by benchmarking the
    computing tasks and uses predefined sizes at run time.
Both \gpustore{} and \snap{} simply use this approach in their
    prototypes.
The dynamic way would do microbenchmarking at run time to find optimal
    sizes for the specific execution hardware and software
    environment. It can adjust workload sizes according to the
    hardware and system load, and hence is more accurate.


\section{Asynchronous GPU Programming}
\label{sec:asynchronous-gpu-programming}

CUDA stream requires asynchronous DMA memory copy and GPU kernel
    launching operations so that a single CPU thread can launch
    multiple streams.
However, current GPU programming model needs the synchronization between the
    host and the device to detect the completion of the device
    operations in each stream.
    \ignore{as showed in the following typical CUDA
    program code:
\begin{lstlisting}[language=C]
  for (i=0; i<nrStreams; ++i) {
    cudaMemcpyAsync(devBuf[i], hostBuf[i], sz, stream[i], ...);
    gpuKernel<<<...,stream[i],..>>>(...);
    cudaMemcpyAsync(hostRet[i], devRet[i], retsz, stream[i], ...);
  }
  ...
  cudaDeviceSynchronize(0);
\end{lstlisting}
In the code snippet above, all memory copy and GPU kernel launching are
    asynchronous calls, but the necessary
    \texttt{cudaDeviceSynchronize} call blocks the asynchronous
    control flow.}
This may not be a problem at all for traditional GPGPU computing that
    just focuses on a particular computing task.
However, many system-level components are designed to exploit
    asynchrony in order to achieve high performance.
    
For example, filesystems work with the virtual file system (VFS) layer,
    and often
rely on the OS kernel page-cache for reading and writing. By its
    nature,
the page-cache makes all I/O operations asynchronous: read and write
requests to the page cache are not synchronous unless an explicit
\texttt{sync} operation is called or a \emph{sync} flag is set when
opening a file.

Other examples include the virtual block device drivers, which work with the OS
    kernel's block I/O layer.
This layer is an asynchronous request processing system.  Once
    submitted,
block I/O requests are maintained in queues. Device drivers, such as
small computer system interface (SCSI) drivers, are responsible for processing the queue and
invoking callbacks when the I/O is complete.

Some filesystems and block devices, such as network file system (NFS),
    common Internet file system (CIFS), and iSCSI (Internet SCSI),
depend on the network stack to provide their functionality.  Because
of the high and unpredictable latency on a network,
these subsystems are asynchronous by necessity.

When it goes to the network packet processing, it is totally
    asynchronous workflow. Although there does exist totally
    synchronous network stacks such as the uIP~\cite{uIP} and
    lwIP~\cite{lwIP}, they are designed for memory constraint
    embedded systems, not for performance.
As a result, the synchronization CUDA call is totally unacceptable in
    those asynchronous environment because it will block the normal
    workflow and may cause performance slow down.

\gpustore{} and \snap{} solve this problem by implementing
    asynchronous CUDA stream callback mechanism to enable completely
    asynchronous GPU program control flow.
Such callback mechanism can be implemented in two different
    approaches.
    \begin{description}
      \item[Polling-based.] this method has a thread keep polling stream
        state change and invoke callbacks. It is obvious that this
        approach can get low latency response but will keep a CPU core
        in busy waiting loop.
      \item[Signal-based.] this is implemented with the events invoked
        by GPU interrupts at the low level. The signal-based response
        latency is definitely higher than the polling-based one, but
        its advantage is also obvious: CPU cores can be freed to
        process other work without busy waiting.
    \end{description}
At the time of writing this dissertation, the latest CUDA release has provided
    similar signal-based callback for streams after \gpustore{} did that for more than two
    years.
This further confirms the effectiveness of the techniques we've proposed.    

\section{Reduce Memory Copy Overhead}
\label{sec:reducing-memcpy}

Having said at the beginning of this chapter, the memory copy is often
    the major overhead compared with the computation.
Many different aspects are related to the performance of memory copy.
Since we are discussing system-level GPU computing, the memory copy
    is not only the DMA over PCIe bus. We also need to consider what
    happens in the host main memory as an entire system.
We will start from the obvious challenges and problems in GPU related
    memory copy, then gradually introduce other vital issues and
    techniques to deal with them.

\subsection{DMA Overhead}
\label{sec:reducing-dma-overhead}

The overhead of DMA comes from its special requirement: the memory
    pages in main memory must not be swapped out (paging) during DMA.
To satisfy such requirement when copying data in pageable memory to
    GPUs, CUDA GPU drivers use the aforementioned double-buffering
    approach as described in Section~\ref{sec:advanced-gpgpu}.
According to the evaluations~\cite{OptimizeCUDAMemcpy},
    double-buffering DMA can be two times slower than using
    page-locked memory directly.
Compared with pageable memory, using page-locked memory may lock too
    many physical pages and reduce the available memory for other
    applications.
However, considering today's cheap DRAM and widely available tens of
    gigabytes DRAM on a single machine, locking even 6GB memory (for a
    high end GPU~\cite{TitanBlack}) is totally
    acceptable.

\subsection{Avoid Double-buffering}
\label{sec:avoid-double-buffering}

The aforementioned solution with CUDA
    page-locked memory requires the system code to use CUDA-allocated
    memory as its data buffer.
This seems trivial: traditional GPGPU computing usually reads the data
    from a file or the network into host memory, copies the entire
    data buffer into GPU device memory, and does the computation.
It is seldom that the host memory used in this scenario may have any
    other users or complicated dependencies, so it is easy to replace
    it with CUDA-allocated page-locked memory.
However, the code in a system component often works as a stage of a
    long data processing pipeline.
In that case, it may be impractical to modify the entire system from
    the beginning of the pipeline to use CUDA's memory, especially in
    a large complex system such as the operating system kernel.
One way to deal with this is allocating a separate page-locked buffer
    with CUDA, and copy the data to be processed into this CUDA buffer
    before DMA (and similar approach for the processing result).
This leads to double-buffering, which is similar to the aforementioned
    early stage CUDA DMA implementation for pageable memory:
    introducing extra copy in host memory.
To avoid the double-buffering problem, we've proposed and implemented
    the page remapping technique, which can remap external
    page-locked memory pages into CUDA GPU driver's memory area, and
    make them DMA-capable just like CUDA page-locked memory (refer to
    Section~\ref{sec:gpustore-mm}.)
This allows minimum invasive GPU computing integration into an
    existing complex system: only the component containing the
    computing tasks needs a few modifications.

There do exist some special cases where the component we'd like to
    put GPU-accelerated computing tasks into may be the beginning of
    the data processing pipeline. It may also be a pipeline stage that allocates memory for
    later use.
In those cases, replacing previous \texttt{malloc} or similar memory
    allocators with CUDA's memory allocation functions is feasible and
    efficient.
Introducing remapping in those cases doesn't make any sense due to the
    added complexity of the extra page mappings.

The techniques in this section try to avoid memory copy in host memory
    because such copy is not necessary.
However, there is an implicit assumption that the data in the memory
    buffer are all useful to the computation. Otherwise it may be
    unwise to copy the entire buffer to device since it wastes the
    relatively slow PCIe bandwidth, as we shall discuss in the
    following section.

\subsection{Trade Off Memory Copy Costs}
\label{sec:trade-off-different-memcpy-costs}

Some computing tasks in a system component may need only small pieces
    of a large data unit it receives.
Typical examples are the packet processing tasks: IP routing just
    needs the destination IP address, layer 2 forwarding just needs
    the destination's media access control (MAC) address, packet
    classification just needs five small fields, etc.
Copying the entire data unit into GPU memory can waste a large portion
    of the PCIe bus bandwidth: considering the 4 bytes IP address
    versus the minimum 64 bytes packet.
At the same time, the host side memory bandwidth is much faster
    than the PCIe bus.
Take a look at the machine I used for \snap{} evaluation: the main
    memory has a $38.4$GB/s bandwidth, while the maximum throughput of
    the PCIe $2.0$ 16x slot is $8$GB/s in each direction, which is a
    $4.8$ times difference.
As a result, for computing tasks with the data usage patterns
    discussed here, copying the needed data pieces into a much smaller
    page-locked memory buffer and then launching a PCIe DMA for this
    small buffer may lead to a much faster total memory copy than
    copying the entire data unit into GPU memory through PCIe bus.
The aforementioned \snap{}'s the ROI-based slicing
    technique is based on this idea. It takes advantage of the
    much faster host memory to achieve fast host-device memory copy.
As we mentioned in
    Section~\ref{sec:coalescing-global-memory-accesses}, it also makes
    coalescing-friendly data structures for GPUs (refer to
    Chapter~\ref{chap:snap} for details).

\subsection{Discussion}
\label{sec:memcpy-discussion}

Section~\ref{sec:trade-off-different-memcpy-costs} advocates a
    technique that is totally opposite to the one in its previous
    section (Section~\ref{sec:avoid-double-buffering}).
But they are not paradoxical.
It is the data usage pattern of the computing task that decides which
    technique to use.
On the one hand, for a computing task that needs the entire data
    unit received by the enclosing system for its computation, it
    should avoid extra buffers in host memory for the same data unit
    content with either the remapping technique or using CUDA
    page-locked memory directly, depending on the memory management 
    role of the system as we discussed in
    Section~\ref{sec:avoid-double-buffering}.
On the other hand, a computing task needing only small pieces of the
    entire data unit should trade off the main memory bandwidth and
    the host-device memory copy bandwidth, and may use techniques
    similar to \snap{}'s ROI-based slicing for faster total memory
    copy performance.
\snap{}'s ROI-based slicing is a very flexible data abstraction to
    deal with both kinds of usage patterns: it allows computing tasks
    to define their own pieces to be processed in a single data unit,
    for a ``use all'' usage pattern, the computing task can specify
    the entire data unit as its interested region.


\section{Miscellaneous Challenges}
\label{sec:misc-challenges}

There are some challenges caused by the limitations in current GPU
    computing libraries or GPU hardware.
Though we can believe that with the development of GPU technology and
    the GPU market, these obstacles may disappear in future, it is
    still worth describing them here for readers who are trying to
    apply GPU computing into system components at this time.
The challenges discussed in this section are only related to our GPU
    computing experiences learned from \gpustore{} and \snap{}, the
    survey chapter (in Chapter~\ref{chap:survey}) discusses more such
    challenges and solutions provided by other system-level GPU
    computing works.

\subsection{Kernel-User Communication}
\label{sec:ku-comm-for-gpu}

Currently, in almost all GPU computing libraries, drivers are
    closed source, not to mention the even more closed GPU hardware.
This leads to a big performance problem when applying GPU computing
    into an operating system: the OS kernel mode code has to rely on the
    userspace GPU computing library to use GPUs.
So now in OS kernel mode, using GPUs is not as efficient as a function
    call in userspace, but a cross context communication.
Such a system needs an efficient kernel-user communication mechanism for
    invoking GPU computing library functions, and also memory sharing
    between two modes for computing data.
\gpustore{} got this problem on Linux kernel when using CUDA GPU
    library.
Current open source GPU drivers such as nouvea~\cite{Nouvea} and open
    source CUDA implementation such as Gdev~\cite{Gdev} still can't
    reach the proprietary software's performance.
\gpustore{} uses a userspace helper to deal with requests from OS kernel
    and invoke CUDA calls.
The userspace helper is based on polling-based file event mechanism to
    achieve fast kernel-user communication.
The details are in Chapter~\ref{chap:gpustore}.
The Barracuda~\cite{Barracuda} GPU microdriver has evaluated different
    approaches to implement efficient kernel-user communication for
    GPU computing.

\subsection{Implicit Synchronization}
\label{sec:implicit-hd-sync}

The host-device synchronization happens not only when the host side
    explicitly calls \texttt{cudaDeviceSynchronize} function,
    but also when some GPU resource management operations
    are performed~\cite{CUDAProgrammingGuide}.
Such operations include CUDA host or device memory allocation, GPU
    information query, etc.
The host and device memory allocation is the main trouble maker
    because it is almost unavoidable.
This may stall the aforementioned asynchronous GPU programming in
    Section~\ref{sec:asynchronous-gpu-programming} even when there is
    no explicit stream synchronization.
As a result, as we will see in Chapter~\ref{chap:survey}, a common
    technique used by many system-level GPU computing works including
    our \gpustore{} and \snap{} frameworks is to preallocate CUDA
    page-locked memory, and manage the memory allocation on their own.
This can easily consume a lot of memory, but due to the current GPU
    limitations, it is a must to achieve asynchronous GPU
    computing. 
    
\section{Summary}
\label{sec:sysgpu-summary}

In this chapter, we discussed generic system-level challenges and also
    proposed high-level principles and techniques to deal with them.
The proposed principles and techniques are designed to make system
    code efficiently work with the throughput-oriented GPU
    architecture, take advantage of the wide GPU memory interface, do
    nonblocking host and device communication and reduce unnecessary
    overheads during GPU DMA.   
These principles are mainly about batching to provide parallel
    workloads, truly asynchronously programming GPUs with callbacks or
    status polling at the CPU side, compacting workload data to reduce
    unnecessary PCIe transfer, and using locked memory directly to
    avoid double-buffering DMA.
In the next two chapters, we will discuss our two concrete frameworks:
    \gpustore{} and \snap{}, to explain how we apply these generic
    principles and techniques in practice to deal with their specific
    problems.
    


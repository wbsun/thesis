## -*- mode: Makefile -*-

all: doc

draft: *.tex *.sty *.cls *.bib
	pdflatex "\def\makedraftversion{}\input{thesis}"
	bibtex thesis
	pdflatex "\def\makedraftversion{}\input{thesis}"
	pdflatex "\def\makedraftversion{}\input{thesis}"

doc: *.tex *.sty *.cls *.bib
	pdflatex thesis
	bibtex thesis
	pdflatex thesis
	pdflatex thesis

clean::
	$(RM) *.aux *.bbl *.blg *.dvi *.log *.out *.synctex.gz *.lof *.lot *.toc

realclean:: clean
	$(RM) *.eps *.pdf *.ps

